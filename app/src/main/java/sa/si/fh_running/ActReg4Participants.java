package sa.si.fh_running;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ActReg4Participants extends AppCompatActivity {

    private static int numb=0;
    private String act4FirstName="Vorname";
    private String act4LastName="Nachname";
    private String act4RFID="0";
    private String sqlStatement;
    private String act4TimeStart="";
    private String act4TimeFinish="";
    private String TabName="Name";
    private String act4ID="ID";

    private SQLiteDatabase database; //nö...

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_reg_4_participants);

        //Toolbar
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar); //Getting own Toolbar
        TextView test = (TextView) toolbar.findViewById(R.id.toolbarTitle);
        ImageView logoChange=(ImageView) toolbar.findViewById(R.id.toolbarLogo);

        logoChange.setImageDrawable(Drawable.createFromPath("fh_timing"));
        test.setText("Registrierung");


        //Intents to get Data from other Activities
        Intent intent = getIntent();

        numb=intent.getIntExtra("regPartNum",0);    //Test
        act4FirstName=intent.getStringExtra("regFirstName");    //Test
        act4LastName=intent.getStringExtra("regLastName");      //Test
        act4RFID=intent.getStringExtra("regRFIDCode");
        TabName=intent.getStringExtra("TableName");
        act4ID=intent.getStringExtra("regAccID");               //Test


        //TextViews
        TextView partFirstName = (TextView) findViewById(R.id.Act4RegPartFirstName);
        TextView partLastName = (TextView) findViewById(R.id.Act4RegPartLastName);
        TextView partNumb = (TextView) findViewById(R.id.Act4RegPartNum);

        //Set Infos
        partNumb.setText(""+numb);
        partFirstName.setText(act4FirstName);
        partLastName.setText(act4LastName);

        Button act4ButNext = (Button) findViewById(R.id.Act4RegButtonNext);

        act4ButNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sqlStatement=createSQLStatement(TabName, act4FirstName, act4LastName, act4RFID, act4ID, numb, act4TimeStart, act4TimeFinish );
                //if(SQL(sqlStatement, database)) {
                    Toast.makeText(ActReg4Participants.this,"Regisitrierung erfolgreich", Toast.LENGTH_LONG).show();
                    openActMain();
                /*}

                else {
                    Toast.makeText(ActReg4Participants.this,"Ein Fehler ist aufgetreten", Toast.LENGTH_LONG);
                }*/
            }
        });
    }

    protected String createSQLStatement(String TabName, String firstNameIn, String lastNameIn, String rfidIn, String idIn, int numbIn, String timeStart, String timeEnd ) {
        //String values;
        String values = "(" + idIn + "," + rfidIn + "," + firstNameIn + "," + lastNameIn+"," + timeStart + "," + timeEnd + ")";
        String msg = "INSERT INTO " + TabName + " (ID, RFID, FirstName, SecondName, TimeStart, TimeEnd) VALUES " + values;

        return msg;
    }

    protected boolean SQL (String msg, SQLiteDatabase database) {
        database.execSQL(msg);

        return true;
    }

    protected void openActMain(){
        Intent intent = new Intent(this, ActMainMenue.class);


        startActivity(intent);
        finish();
    }
}
