package sa.si.fh_running;

public class Runner_time_signal implements Comparable<Runner_time_signal>  {

    private int dSignalStrength;
    public String sTime;

    Runner_time_signal(int signalStrengthIn, String timeIn) {
        this.dSignalStrength = signalStrengthIn;
        this.sTime = timeIn;
    }

    public String GetTime() {
        return this.sTime;
    }

    public String GetSignalStrength() { return String.valueOf(this.dSignalStrength); }

    @Override
    public int compareTo(Runner_time_signal f) {
        if (dSignalStrength > f.dSignalStrength)
            return -1;
        else if (dSignalStrength < f.dSignalStrength)
            return 1;
        else
            return 0;
    }
}
