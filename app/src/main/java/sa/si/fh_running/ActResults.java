package sa.si.fh_running;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

public class ActResults extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_results);

        //Toolbar
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar); //Getting own Toolbar

        //Setting title
        TextView test = (TextView) toolbar.findViewById(R.id.toolbarTitle);
        test.setText("Start Event");
    }

}
