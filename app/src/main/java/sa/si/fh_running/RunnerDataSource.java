package sa.si.fh_running;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

public class RunnerDataSource {
    private SQLiteDatabase db;
    private RunnerDbHelper runnerDbHelper;
    private String eventName;
    private String[] allColumns = {"RunnerID", "RunnerRFID", "RunnerStartTime", "RunnerFinishTime"};

    public RunnerDataSource(Context context,String eventNameIn) {
        eventName=eventNameIn;
        runnerDbHelper = new RunnerDbHelper(context,eventName);
    }

    public void open() throws SQLException {
        db=runnerDbHelper.getWritableDatabase();
    }

    public void close() {
        runnerDbHelper.close();
    }

    public Runner_time_id_signal createEntry(String runnerRFID, String runnerStartTime, String runnerFinishTime) {
        ContentValues values = new ContentValues();
        values.put("RunnerRFID",runnerRFID);
        values.put("RunnerStartTime",runnerStartTime);
        values.put("RunnerFinishTime", runnerFinishTime);

        long insertID = db.insert(eventName,null, values);

        Cursor cursor = db.query(eventName,allColumns,"RunnerID = "+insertID,null,null,null,null);
        cursor.moveToFirst();

        return cursorToEntry(cursor);
    }

    public Runner_time_id_signal updateEntry(String runnerRFID, String runnerFinishTime) {
        ContentValues values = new ContentValues();
        values.put("RunnerFinishTime",runnerFinishTime);

        long insertID=db.update(eventName,values,"RunnerRFID = ?",new String[]{runnerRFID});

        Cursor cursor = db.query(eventName,allColumns,"RunnerID = "+insertID,null,null,null,null);
        cursor.moveToFirst();

        return cursorToEntry(cursor);
    }

    protected Runner_time_id_signal cursorToEntry(Cursor cursor) {
        Runner_time_id_signal runner_time_id_signal = new Runner_time_id_signal();
        runner_time_id_signal.setDbId(cursor.getInt(0));
        runner_time_id_signal.setRfidId(cursor.getString(1));
        runner_time_id_signal.setStartTime(cursor.getString(2));
        runner_time_id_signal.setFinishTime(cursor.getString(3));

        //kA Warum hier, aber das is halt mal so!

        return runner_time_id_signal;
    }

    protected List<Runner_time_id_signal> getAllEntries(String eventName) {
        List<Runner_time_id_signal> RunnerTimeIdList = new ArrayList<Runner_time_id_signal>();

        Cursor cursor = db.query(eventName,allColumns,null,null,null,null,null);
        cursor.moveToFirst();

        if(cursor.getCount() == 0 )
            return RunnerTimeIdList;

        while (cursor.isAfterLast() == false) {
            Runner_time_id_signal Runner = cursorToEntry(cursor);
            RunnerTimeIdList.add(Runner);
            cursor.moveToNext();
        }
        cursor.close();

        return RunnerTimeIdList;
    }       //for reading

}
