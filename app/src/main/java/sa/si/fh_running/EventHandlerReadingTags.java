package sa.si.fh_running;

import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

import com.zebra.rfid.api3.ACCESS_OPERATION_CODE;
import com.zebra.rfid.api3.ACCESS_OPERATION_STATUS;
import com.zebra.rfid.api3.HANDHELD_TRIGGER_EVENT_TYPE;
import com.zebra.rfid.api3.InvalidUsageException;
import com.zebra.rfid.api3.OperationFailureException;
import com.zebra.rfid.api3.RFIDReader;
import com.zebra.rfid.api3.ReaderDevice;
import com.zebra.rfid.api3.Readers;
import com.zebra.rfid.api3.RfidEventsListener;
import com.zebra.rfid.api3.RfidReadEvents;
import com.zebra.rfid.api3.RfidStatusEvents;
import com.zebra.rfid.api3.STATUS_EVENT_TYPE;
import com.zebra.rfid.api3.TagData;

import java.util.ArrayList;

public class EventHandlerReadingTags implements RfidEventsListener {

    private static RFIDReader reader;
    static String TAG = "DEMO";
    private Handler mHandler;

    public EventHandlerReadingTags(RFIDReader readerIn, Handler handlerIn) {
        this.reader=readerIn;
        this.mHandler=handlerIn;
    }


    public void eventReadNotify(RfidReadEvents e) {
        TagData[] myTags = reader.Actions.getReadTags(1);

        if (myTags != null) {
            for (int i=0; i<myTags.length; i++) {
                String msg = myTags[i].getTagID();
                Log.d(TAG, "Tag ID "+myTags[i].getTagID());

                System.out.println("Test it if it is right: "+msg);

                if (myTags[i].getOpCode() == ACCESS_OPERATION_CODE.ACCESS_OPERATION_READ && myTags[i].getOpStatus() == ACCESS_OPERATION_STATUS.ACCESS_SUCCESS){
                    if (myTags[i].getMemoryBankData().length() > 0) {
                        Log.d(TAG, "Mem Bank Data " + myTags[i].getMemoryBankData());
                    }
                }
            }
        }
    }

    //status event notification
    public void eventStatusNotify(RfidStatusEvents rfidStatusEvents) {
        Log.d (TAG, "Status Notification: " + rfidStatusEvents.StatusEventData.getStatusEventType());

        if (rfidStatusEvents.StatusEventData.getStatusEventType() == STATUS_EVENT_TYPE.HANDHELD_TRIGGER_EVENT) {
            if(rfidStatusEvents.StatusEventData.HandheldTriggerEventData.getHandheldEvent() == HANDHELD_TRIGGER_EVENT_TYPE.HANDHELD_TRIGGER_PRESSED){

                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... voids) {
                        try {
                            reader.Actions.Inventory.perform();
                        } catch (InvalidUsageException e) {
                            e.printStackTrace();
                        } catch (OperationFailureException e){
                            e.printStackTrace();
                        }
                        return null;
                    }
                }.execute();
            }

            if(rfidStatusEvents.StatusEventData.HandheldTriggerEventData.getHandheldEvent() == HANDHELD_TRIGGER_EVENT_TYPE.HANDHELD_TRIGGER_RELEASED){

                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... voids) {
                        try {
                            reader.Actions.Inventory.stop();
                        } catch (InvalidUsageException e) {
                            e.printStackTrace();
                        } catch (OperationFailureException e){
                            e.printStackTrace();
                        }
                        return null;
                    }
                }.execute();
            }
        }
    }
}
