package sa.si.fh_running;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.zebra.rfid.api3.ENUM_TRANSPORT;
import com.zebra.rfid.api3.ENUM_TRIGGER_MODE;
import com.zebra.rfid.api3.HANDHELD_TRIGGER_EVENT_TYPE;
import com.zebra.rfid.api3.InvalidUsageException;
import com.zebra.rfid.api3.OperationFailureException;
import com.zebra.rfid.api3.RFIDReader;
import com.zebra.rfid.api3.ReaderDevice;
import com.zebra.rfid.api3.Readers;
import com.zebra.rfid.api3.RfidEventsListener;
import com.zebra.rfid.api3.RfidReadEvents;
import com.zebra.rfid.api3.RfidStatusEvents;
import com.zebra.rfid.api3.START_TRIGGER_TYPE;
import com.zebra.rfid.api3.STATUS_EVENT_TYPE;
import com.zebra.rfid.api3.STOP_TRIGGER_TYPE;
import com.zebra.rfid.api3.TagData;
import com.zebra.rfid.api3.TriggerInfo;

import org.w3c.dom.Text;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class ActStart1Event extends AppCompatActivity {

    public static Readers readers;
    private static ReaderDevice readerDevice;
    public static RFIDReader reader;
    private static ArrayList<ReaderDevice> availableRFIDReaderList;
    Boolean killTimer=true;

    //Handler mHandler = new Handler(){};

    TextView systemTime;//=findViewById(R.id.ActStart1_time);
    TextView systemDate;
    DateFormat df_time=new SimpleDateFormat("HH:mm:ss");
    DateFormat df_date=new SimpleDateFormat("dd.MM.yyyy");
    //TimeUpdate kA = new TimeUpdate();

    //Handler

    //Eventhandler for RFID-Events
    public EventHandlerReadingTagsReg eventHandler;

    //Handler for message transfer
    private Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message inMessage) {
            switch (inMessage.what) {
                case 1:
                    killTimer=false;//mHandler.obtainMessage(3,false).sendToTarget();
                    MediaPlayer startGun = MediaPlayer.create(ActStart1Event.this, R.raw.start_gun);
                    try {
                        reader.Events.removeEventsListener(eventHandler);
                    } catch (InvalidUsageException e) {
                        e.printStackTrace();
                    } catch (OperationFailureException e) {
                        e.printStackTrace();
                    }
                    startGun.start();
                    openActivity();
                    break;
                case 2:
                    //String newTime = inMessage.obj.toString();
                    systemTime.setText(inMessage.obj.toString());
            }
        }
    };

    //Location Services for next Activity
    public static LocationManager locationManager;
    public static LocationListener locationListener;
    private long delta=0;

    @SuppressLint("MissingPermission")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_start_1_event);

        //Toolbar
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar); //Getting own Toolbar

        //Setting title
        TextView test = (TextView) toolbar.findViewById(R.id.toolbarTitle);
        test.setText("Start Event");


        //TextViews
        systemTime = (TextView) findViewById(R.id.ActStart1_time);
        systemDate = (TextView) findViewById(R.id.ActStart1_date);

        //Setting default Values for Time and Date
        Date date=new Date();
        systemDate.setText(df_date.format(date));
        systemTime.setText((Calendar.getInstance().getTime()).toString());

        //connecting to GNSS Satelite and update delta
        locationManager= (LocationManager) getSystemService(LOCATION_SERVICE);
        locationListener= new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                DateFormat dateFormat2=new SimpleDateFormat("HHmmssSSS", Locale.GERMANY);
                delta=Long.parseLong(dateFormat2.format(location.getTime())) - Long.parseLong(dateFormat2.format(Calendar.getInstance().getTime()));
                System.out.println("Delta: "+delta);
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);

        AddReader();
        TimeUpdate();
    }

    public void TimeUpdate() {
        new AsyncTask<Void, Void, Void>() {
            //int t = 0;

            @Override
            protected Void doInBackground(Void... voids) {
                while (killTimer) {
                        mHandler.obtainMessage(2, df_time.format(Calendar.getInstance().getTime())).sendToTarget();
                        //timeStamp=df_time.format(setTimer);
                        //t++;
                            try {
                            Thread.sleep(1000);
                        } catch(InterruptedException e){
                            e.printStackTrace();
                        }

                }
                return null;
            }
        }.execute();
    }

    public void AddReader() {
        if (readers == null) {
            readers = new Readers(this, ENUM_TRANSPORT.BLUETOOTH);
        }

        new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... voids) {
                try {
                    if (readers != null) {
                        if (readers.GetAvailableRFIDReaderList() != null) {
                            availableRFIDReaderList = readers.GetAvailableRFIDReaderList();

                            if (availableRFIDReaderList.size() != 0) {
                                readerDevice = availableRFIDReaderList.get(0);
                                reader = readerDevice.getRFIDReader();

                                if (!reader.isConnected()) {
                                    reader.connect();
                                    ConfigureReader();
                                    return true;
                                }
                            }
                        }
                    }
                } catch (InvalidUsageException e) {
                    e.printStackTrace();
                } catch (OperationFailureException e) {
                    e.printStackTrace();
                    //Log.d(TAG, "OperationFailureException " + e.getVendorMessage());
                }
                return false;
            }
        }.execute();
    }

    private void ConfigureReader() {
        if (reader.isConnected()) {
            TriggerInfo triggerInfo = new TriggerInfo();

            triggerInfo.StartTrigger.setTriggerType(START_TRIGGER_TYPE.START_TRIGGER_TYPE_IMMEDIATE);
            //triggerInfo.StopTrigger.setTriggerType(STOP_TRIGGER_TYPE.STOP_TRIGGER_TYPE_IMMEDIATE);

            try {
                //receive events from device
                if(eventHandler == null) {
                    eventHandler=new EventHandlerReadingTagsReg(reader);
                }

                reader.Events.addEventsListener(eventHandler);
                reader.Events.setHandheldEvent(true); //Get Handheld events

                reader.Config.setTriggerMode(ENUM_TRIGGER_MODE.RFID_MODE, false);    // set trigger mode as rfid -> disable barcode beams

                reader.Config.setStartTrigger(triggerInfo.StartTrigger);
                //reader.Config.setStopTrigger(triggerInfo.StopTrigger);

            } catch (InvalidUsageException e) {
                e.printStackTrace();
            } catch (OperationFailureException e) {
                e.printStackTrace();
            }
        }
    }

    void openActivity() {
        Intent intent = new Intent(this, ActStart2Event.class);
        System.out.println(delta);
        intent.putExtra("delta", delta);

        locationManager.removeUpdates(locationListener);
        locationManager=null;
        locationListener=null;
        //intent.putExtra("Readers", (Serializable) readers);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        disconReader();
        finish();
        System.gc();
        super.onDestroy();
    }

    protected void disconReader() {
        if (reader != null) {
            try {
                reader.Events.removeEventsListener(eventHandler);
                reader.disconnect();
                reader = null;
                readers.Dispose();
                readers = null;

                if (mHandler != null) {
                    mHandler = null;
                }
                if (eventHandler != null) {
                    eventHandler = null;
                }
            } catch (InvalidUsageException e) {
                e.printStackTrace();
            } catch (OperationFailureException e) {
                e.printStackTrace();
            }
        }
    }

    public void butActStart2_next(View view) {
        //killTimer=false;//mHandler.obtainMessage(3,false).sendToTarget();
        MediaPlayer startGun = MediaPlayer.create(ActStart1Event.this, R.raw.start_gun);
        try {
            reader.Events.removeEventsListener(eventHandler);
        } catch (InvalidUsageException e) {
            e.printStackTrace();
        } catch (OperationFailureException e) {
            e.printStackTrace();
        }
        startGun.start();
        openActivity();

    }

    public class EventHandlerReadingTagsReg implements RfidEventsListener {

        private RFIDReader reader;

        public EventHandlerReadingTagsReg(RFIDReader readerIn) {
            this.reader = readerIn;
        }

        public void eventReadNotify(RfidReadEvents e) {

        }

        //status event notification
        public void eventStatusNotify(RfidStatusEvents rfidStatusEvents) {

            //Notification that trigger was pressed
            if (rfidStatusEvents.StatusEventData.getStatusEventType() == STATUS_EVENT_TYPE.HANDHELD_TRIGGER_EVENT) {
                if(rfidStatusEvents.StatusEventData.HandheldTriggerEventData.getHandheldEvent() == HANDHELD_TRIGGER_EVENT_TYPE.HANDHELD_TRIGGER_PRESSED) {

                    mHandler.obtainMessage(1).sendToTarget();
                }
            }
        }
    }
}
