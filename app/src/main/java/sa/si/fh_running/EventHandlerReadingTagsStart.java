package sa.si.fh_running;

import android.annotation.SuppressLint;
import android.util.Log;

import com.zebra.rfid.api3.InvalidUsageException;
import com.zebra.rfid.api3.OperationFailureException;
import com.zebra.rfid.api3.RFIDReader;
import com.zebra.rfid.api3.Readers;
import com.zebra.rfid.api3.RfidEventsListener;
import com.zebra.rfid.api3.RfidReadEvents;
import com.zebra.rfid.api3.RfidStatusEvents;
import com.zebra.rfid.api3.TagData;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import static java.lang.Thread.sleep;

public class EventHandlerReadingTagsStart implements RfidEventsListener {
    private RFIDReader reader;
    private DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss.SSS",Locale.GERMANY);

    private String timeStamp;
    private long delta;

    List<Runner_time_id_signal> time_id_signals;

    //String time=dateFormat.format(Calendar.getInstance().getTime()+delta);
    Boolean bCheck=false;

    public EventHandlerReadingTagsStart(RFIDReader readerIn, long deltaIn, String timeStampIn, List<Runner_time_id_signal> runner_time_id_signalIn) {
        this.reader = readerIn;
        this.delta=deltaIn;
        this.timeStamp=timeStampIn;
        this.time_id_signals=runner_time_id_signalIn;
    }

    @SuppressLint("MissingPermission")
    @Override
    public void eventReadNotify(RfidReadEvents rfidReadEvents) {

        TagData[] myTags = reader.Actions.getReadTags(1000);

        Runner_time_id_signal runner;
        boolean tagAd=true;

        //Stopping inventory to perform Rest
        try {
            reader.Actions.Inventory.stop();
        } catch (InvalidUsageException | OperationFailureException e) {
            e.printStackTrace();
        }

        //time calc with correction
        long timeCalc= Calendar.getInstance().getTimeInMillis() + delta;   //Getting system-time and adding correction to get GNSS time
        timeStamp=dateFormat.format(timeCalc);                                                  //setting readable format

        for(TagData actTag:myTags) {
            tagAd = true;
            runner = new Runner_time_id_signal(actTag.getTagID(), actTag.getPeakRSSI(), timeStamp);

            if (time_id_signals==null){
                time_id_signals=new ArrayList<Runner_time_id_signal>();
                time_id_signals.add(runner);
            } else {
                for (Runner_time_id_signal runnerCheck : time_id_signals) {
                    if (runnerCheck.getRfidId().equals(actTag.getTagID())) {
                        runnerCheck.setNewTimeSignal(actTag.getPeakRSSI(), timeStamp);
                        tagAd=false;
                        break;
                    }
                }
                if(tagAd) {
                    time_id_signals.add(runner);
                }
            }
        }

        reader.Actions.purgeTags();

        try {
            sleep(200);
            reader.Actions.Inventory.perform();
        } catch (InvalidUsageException e) {
            e.printStackTrace();
        } catch (OperationFailureException e) {
            e.printStackTrace();
            Log.d("2","Inventory problems");
        } catch (InterruptedException e) {
            e.printStackTrace();
            Log.d("1","Problem in Sleep");
        }
/*
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... voids) {
                    try {
                        sleep(200);
                        reader.Actions.purgeTags();
                        reader.Actions.Inventory.perform();

                    } catch (InvalidUsageException e) {
                        e.printStackTrace();
                    } catch (OperationFailureException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    return null;
                }
            }.execute();

            */
    }

    @Override
    public void eventStatusNotify(RfidStatusEvents rfidStatusEvents) {

    }

    public List<Runner_time_id_signal> getTime_id_signals() {
        return time_id_signals;
    }
}
