package sa.si.fh_running;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class ActReg3Participants extends AppCompatActivity {

    public int numb=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_reg_3_participants);


        //Toolbar
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar); //Getting own Toolbar
        TextView test = (TextView) toolbar.findViewById(R.id.toolbarTitle);
        ImageView logoChange=(ImageView) toolbar.findViewById(R.id.toolbarLogo);

        logoChange.setImageDrawable(Drawable.createFromPath("fh_timing"));
        test.setText("Registrierung");

        //Edit TextView
        final EditText editText = (EditText) findViewById(R.id.textInNumb);

        //button
        Button butAct3RegPartConcl = (Button) findViewById(R.id.butAct3RegPartConcl);
        butAct3RegPartConcl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!editText.toString().isEmpty())
                {
                    numb=Integer.parseInt(editText.getText().toString());
                    openActivityMain();
                }
            }
        });

    }
    protected void openActivityMain() {
        Intent inInten = getIntent();

        Intent intent = new Intent(ActReg3Participants.this, ActReg4Participants.class);
        intent.putExtra("regFirstName",inInten.getStringExtra("regFirstName"));
        intent.putExtra("regLastName",inInten.getStringExtra("regLastName"));
        intent.putExtra("regAccID", inInten.getStringExtra("regAccID"));
        intent.putExtra("regRFIDCode","TestIt");//inIntent.getStringExtra("regRFIDCode"));
        intent.putExtra("TableName", inInten.getStringExtra("TableName"));
        intent.putExtra("regPartNum", numb);
        startActivity(intent);
        finish();
    }
}
