package sa.si.fh_running;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.zebra.rfid.api3.BEEPER_VOLUME;
import com.zebra.rfid.api3.ENUM_TRANSPORT;
import com.zebra.rfid.api3.ENUM_TRIGGER_MODE;
import com.zebra.rfid.api3.HANDHELD_TRIGGER_EVENT_TYPE;
import com.zebra.rfid.api3.InvalidUsageException;
import com.zebra.rfid.api3.OperationFailureException;
import com.zebra.rfid.api3.RFIDReader;
import com.zebra.rfid.api3.ReaderDevice;
import com.zebra.rfid.api3.Readers;
import com.zebra.rfid.api3.RfidEventsListener;
import com.zebra.rfid.api3.RfidReadEvents;
import com.zebra.rfid.api3.RfidStatusEvents;
import com.zebra.rfid.api3.START_TRIGGER_TYPE;
import com.zebra.rfid.api3.STATUS_EVENT_TYPE;
import com.zebra.rfid.api3.TagData;
import com.zebra.rfid.api3.TriggerInfo;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import static java.lang.Thread.sleep;

public class ActFinish2Event extends AppCompatActivity {

    //RFID-Reader Attributes
    private Readers readers;
    private ReaderDevice readerDevice;
    private RFIDReader reader;
    private ArrayList<ReaderDevice> availableRFIDReaderList;
    //Boolean killTimer=true;

    private List<String> timeInList;        //Time Array for pressed trigger time

    private DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss.SSS",Locale.GERMANY);
    private DateFormat dateFormat2 = new SimpleDateFormat("HHmmssSSS",Locale.GERMANY);
    private String actualTime;

    //Handlers
    EventHandlerReadingTags eventHandler;
    //Handler for message transfer
    private Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message inMessage) {
            switch (inMessage.what) {
                case 1:
                    if (timeInList==null) {
                        timeInList=new ArrayList<String>();
                    }
                    long timeCalc = Calendar.getInstance().getTimeInMillis() + delta;   //Getting system-time and adding correction to get GNSS time
                    timeStamp = dateFormat.format(timeCalc);                                                  //setting readable format

                    timeInList.add(dateFormat.format(timeCalc));
            }
        }
    };


    //Attributes for Runner List
    private List<Runner_time_id_signal> time_id_signals;
    private Runner_time_id_signal help;

    //Location Services for next Activity
    private LocationManager locationManager;
    private LocationListener locationListener;

    //Attributes for Time-Delta-Calculation
    private String timeStamp;
    private long delta;

    //Eventname will have to be replaced!!!
    private String eventName="Event_23012019";

    //Database
    private RunnerDataSource dataSource;

    @SuppressLint("MissingPermission")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_finish2_event);

        //Toolbar
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar); //Getting own Toolbar

        //Setting title
        TextView test = (TextView) toolbar.findViewById(R.id.toolbarTitle);
        test.setText("Finish Event");

        //Getting delta calculated in Activity before
        Intent intent = getIntent();
        delta = intent.getLongExtra("delta",0);

        dataSource=new RunnerDataSource(this,eventName);

        //Getting Location Informations (time) to calculate delta
        locationManager= (LocationManager) getSystemService(LOCATION_SERVICE);
        locationListener= new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                delta=Long.parseLong(dateFormat2.format(location.getTime())) - Long.parseLong(dateFormat2.format(Calendar.getInstance().getTime()));
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {
            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };

        //Getting new delta every 10 seconds
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10000, 0, locationListener);

        //Connecting Reader with included inventory-start
        AddReader();
    }

    public void btnActFinish2_next(View view) throws InvalidUsageException, OperationFailureException {
        //Buttons
        //Button butActFinish2stopEvent = (Button) findViewById(R.id.butActFinish2_stopEvent);

        //Progress Bar
       // ProgressBar progressBar=(ProgressBar) findViewById(R.id.progressBarFinish);

        //Textview for Infotext
        //TextView textView=(TextView)findViewById(R.id.textActFinish2_notificationWait);

        locationManager.removeUpdates(locationListener);

        //butActFinish2stopEvent.setVisibility(View.GONE);
        //progressBar.setVisibility(View.VISIBLE);
        //textView.setVisibility(View.VISIBLE);


        reader.Actions.Inventory.stop();

        disconReader();

        locationManager.removeUpdates(locationListener);
        locationManager=null;
        locationListener=null;

        FillDatabase();

        OpenNextActivity();
    }

    public void AddReader() {
        if (readers == null) {
            readers = new Readers(this, ENUM_TRANSPORT.BLUETOOTH);
        }
        try {
            if (readers != null) {
                if (readers.GetAvailableRFIDReaderList() != null) {
                    availableRFIDReaderList = readers.GetAvailableRFIDReaderList();

                    if (availableRFIDReaderList.size() != 0) {
                        readerDevice = availableRFIDReaderList.get(0);
                        reader = readerDevice.getRFIDReader();

                        if (!reader.isConnected()) {
                            reader.connect();
                            ConfigureReader();
                            reader.Actions.Inventory.perform();
                        }
                    }
                }
            }
        } catch (InvalidUsageException e) {
            e.printStackTrace();
        } catch (OperationFailureException e) {
            e.printStackTrace();
            //Log.d(TAG, "OperationFailureException " + e.getVendorMessage());
        }
    }

    public void FillDatabase() {
        /*ProgressBar
        ProgressBar progressBar =(ProgressBar)findViewById(R.id.progressBarFinish);

        //Button
        Button butActFinish2next =(Button) findViewById(R.id.butActFinish2_nextAct);

        //TextView
        TextView textView = (TextView) findViewById(R.id.textActFinish2_notificationWait);

*/
        //Variables for Progressbar
        int     i=0,
                lengthOfList=time_id_signals.size();

        for (Runner_time_id_signal help : time_id_signals) {

            //Sort List by strongest Signal
            help.sortListTimeSignal_Signal();

            //setting FinishTime after Merging with TriggerTimes
            help.setFinishTime(help.mergeTimeWithTrigger(timeInList));

            //Writing received Data into Database (RFID, StartingTime, 0 for FinishTime)
            try {
                dataSource.open();
                dataSource.updateEntry(help.getRfidId(),help.getFinishTime());
                dataSource.close();
                //progressBar.setProgress((i++)/lengthOfList);

            } catch (Exception ex) {

            }

            //Output for check
            System.out.println("Signal: " + help.getSignal());
            System.out.println("Time: " + help.getTime());
            System.out.println("ID: " + help.getRfidId());

            //clear values
            help.clearTimeSignal();
        }

        //remove items from list
        time_id_signals.clear();

        //progressBar.setVisibility(View.GONE);
        //textView.setVisibility(View.GONE);
        //butActFinish2next.setVisibility(View.VISIBLE);
    }

    public void OpenNextActivity() {

        Intent intent = new Intent(this, ActMainMenue.class);
        startActivity(intent);
    }

    private void ConfigureReader() {
        if (reader.isConnected()) {

            TriggerInfo triggerInfo = new TriggerInfo();

            triggerInfo.StartTrigger.setTriggerType(START_TRIGGER_TYPE.START_TRIGGER_TYPE_IMMEDIATE);
            try {
                //receive events from device
                if(eventHandler == null) {
                    eventHandler=new EventHandlerReadingTags(reader);
                }

                reader.Events.addEventsListener(eventHandler);
                reader.Events.setHandheldEvent(true);   //Get Handheld events
                reader.Events.setTagReadEvent(true);    //Get Tag reading events

                reader.Config.setTriggerMode(ENUM_TRIGGER_MODE.RFID_MODE, true);   // set trigger mode as rfid -> disable barcode beams
                reader.Config.setBeeperVolume(BEEPER_VOLUME.QUIET_BEEP);                            // set beeper to silent

            } catch (InvalidUsageException | OperationFailureException e) {
                e.printStackTrace();
            }
        }
    }

    protected void disconReader() {
        if (reader != null) {
            boolean bCheck = true;
            reader.Actions.purgeTags();
            int i=0;

            while (bCheck) {
                if(reader==null || i>100)
                    break;

                try {
                    reader.Config.setBeeperVolume(BEEPER_VOLUME.MEDIUM_BEEP);
                    reader.Config.setTriggerMode(ENUM_TRIGGER_MODE.RFID_MODE, false);
                    reader.Config.setTriggerMode(ENUM_TRIGGER_MODE.BARCODE_MODE, true);
                    bCheck = false;
                } catch (InvalidUsageException | OperationFailureException e) {
                    e.printStackTrace();
                }

                i++;
            }
            bCheck = true;
            i=0;

            while (bCheck) {

                if(reader==null || i >= 1000) {
                    break;
                }

                try {
                    reader.Events.removeEventsListener(eventHandler);
                    bCheck=false;

                } catch (InvalidUsageException | OperationFailureException e) {
                    e.printStackTrace();
                }

                if(reader.isConnected()&&!bCheck) {
                    try {
                        reader.disconnect();

                    } catch (InvalidUsageException | OperationFailureException e) {
                        e.printStackTrace();
                    }
                    readers.Dispose();
                }
                i++;
            }
            readers = null;
            reader = null;
        }
    }

    class EventHandlerReadingTags implements RfidEventsListener {

        private RFIDReader reader;
        private DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss.SSS", Locale.GERMANY);

        Boolean bCheck = false;

        public EventHandlerReadingTags(RFIDReader readerIn) {
            this.reader = readerIn;
        }

        @SuppressLint("MissingPermission")
        @Override
        public void eventReadNotify(RfidReadEvents rfidReadEvents) {

            TagData[] myTags = reader.Actions.getReadTags(1000);

            Runner_time_id_signal runner;
            boolean tagAd = true;

            //time calc with correction
            long timeCalc = Calendar.getInstance().getTimeInMillis() + delta;   //Getting system-time and adding correction to get GNSS time
            timeStamp = dateFormat.format(timeCalc);                                                  //setting readable format

            for (TagData actTag : myTags) {
                tagAd = true;
                runner = new Runner_time_id_signal(actTag.getTagID(), actTag.getPeakRSSI(), timeStamp);

                if (time_id_signals == null) {
                    time_id_signals = new ArrayList<Runner_time_id_signal>();
                    time_id_signals.add(runner);
                } else {
                    for (Runner_time_id_signal runnerCheck : time_id_signals) {
                        if (runnerCheck.getRfidId().equals(actTag.getTagID())) {
                            runnerCheck.setNewTimeSignal(actTag.getPeakRSSI(), timeStamp);
                            tagAd = false;
                            break;
                        }
                    }
                    if (tagAd) {
                        time_id_signals.add(runner);
                    }
                }
            }

            try {
                sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
                Log.d("1", "Problem in Sleep");
            }
        }

        //Trigger pressed Event
        @Override
        public void eventStatusNotify(RfidStatusEvents rfidStatusEvents) {
            if (rfidStatusEvents.StatusEventData.getStatusEventType() == STATUS_EVENT_TYPE.HANDHELD_TRIGGER_EVENT) {
                if (rfidStatusEvents.StatusEventData.HandheldTriggerEventData.getHandheldEvent() == HANDHELD_TRIGGER_EVENT_TYPE.HANDHELD_TRIGGER_PRESSED) {

                    mHandler.obtainMessage(1).sendToTarget();
                }
            }
        }
    }
}
