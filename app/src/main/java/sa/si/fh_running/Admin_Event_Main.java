package sa.si.fh_running;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


public class Admin_Event_Main extends AppCompatActivity implements View.OnClickListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin__event__main);


        //implements
        Button buttonNewEvent = (Button) findViewById(R.id.buttonNewEvent);
        Button buttonExistingEvent = (Button) findViewById(R.id.buttonExistingEvent);

        //changing Toolbar content
        //Toolbar
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar); //Getting own Toolbar
        TextView title  = (TextView) toolbar.findViewById(R.id.toolbarTitle);
        title.setText("Eventverwaltung");

        //Logo
        ImageView logo = (ImageView) toolbar.findViewById(R.id.toolbarLogo);
        logo.setImageDrawable(Drawable.createFromPath("fh_timing"));

    }

    @Override
    public void onClick(View view) {

        Intent intent=null;

        switch(view.getId()) {
            case R.id.buttonNewEvent:
                intent = new Intent(Admin_Event_Main.this, ActNewEvent.class);
                //openNewEvent();
                break;
            case R.id.buttonExistingEvent:
                Toast.makeText(Admin_Event_Main.this, "No", Toast.LENGTH_SHORT).show();
                intent = new Intent(Admin_Event_Main.this, ActMainMenue.class);
                //openExistingEvent();
                break;
        }
        startActivity(intent);

    }

    public void openNewEvent() {
        Intent intent = new Intent(Admin_Event_Main.this, ActNewEvent.class);
        startActivity(intent);
    }
    public void openExistingEvent() {
        //Functionality
    }
}
