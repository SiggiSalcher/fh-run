package sa.si.fh_running;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class ActFinish1Event extends AppCompatActivity {

    LocationManager locationManager;
    LocationListener locationListener;

    long delta=0;

    @SuppressLint("MissingPermission")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_finish1_event);

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                DateFormat dateformat = new SimpleDateFormat("HHmmssSSS",Locale.GERMANY);
                delta = Long.parseLong(dateformat.format(location.getTime())) - Long.parseLong(dateformat.format(Calendar.getInstance().getTime()));
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,0,0,locationListener);
    }

    public void btnActFinish1_next(View view) {
        Intent intent = new Intent(this,ActFinish2Event.class);
        intent.putExtra("delta",delta);
        startActivity(intent);
    }
}
