package sa.si.fh_running;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class RunnerDbHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "FH_Run.db";
    private static final int DB_VERSION = 1;
    private static final String TBL_COLUMN_ID = "RunnerID";
    private static final String TBL_COLUMN_RFID = "RunnerRFID";
    private static final String TBL_COLUMN_START = "RunnerStartTime";
    private static final String TBL_COLUMN_FINISH = "RunnerFinishTime";

    private String TBL_NAME = "Runner";

    private String SQL_CREATE;

    private SQLiteDatabase db;

    public RunnerDbHelper(Context context, String eventNameIn) {
        super(context, DB_NAME, null, DB_VERSION);
        this.TBL_NAME = eventNameIn;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(commandCreateDb());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TBL_NAME);
        onCreate(db);
    }

    private String commandCreateDb() {
        SQL_CREATE= "CREATE TABLE " + this.TBL_NAME;
        SQL_CREATE += " (" + TBL_COLUMN_ID;
        SQL_CREATE += " INTEGER PRIMARY KEY AUTOINCREMENT, ";
        SQL_CREATE += TBL_COLUMN_RFID + " TEXT, ";
        SQL_CREATE += TBL_COLUMN_START + " TEXT, ";
        SQL_CREATE += TBL_COLUMN_FINISH+ " TEXT)";

        return SQL_CREATE;
    }
}
/*
    public void insertStartIntoDatabase(String rfidIn, String startIn) {
        String SQLCommand;

        SQLCommand = "INSERT INTO "+TBL_NAME;
        SQLCommand += " (" + TBL_COLUMN_RFID ;
        SQLCommand += ", " + TBL_COLUMN_START;
        //SQLCommand += " , " + TBL_COLUMN_FINISH;
        SQLCommand += ") VALUES ";
        SQLCommand += " ("+rfidIn;
        SQLCommand += ", " + startIn;
        //SQLCommand += " , NULL)";
        SQLCommand += ")";

        executeCommand(SQLCommand);
    }

    public void insertFinishIntoDatabase(String rfidIn, String finishIn) {
        String SQLCommand;

        SQLCommand = "UPDATE " + TBL_NAME;
        //SQLCommand += " (" + TBL_COLUMN_RFID ;
        SQLCommand += " (" + TBL_COLUMN_FINISH;
        SQLCommand += ") VALUES (";
        //SQLCommand += " ("+rfidIn;
        SQLCommand += " (" + finishIn;
        SQLCommand += ") ";
        SQLCommand += "WHERE " +TBL_COLUMN_RFID;
        SQLCommand += " = " + rfidIn;

        executeCommand(SQLCommand);
    }

    private void executeCommand(String SQLCommand){
        db.execSQL(SQLCommand);
    }

    private String commandCreateDb() {
        SQL_CREATE= "CREATE TABLE " + this.TBL_NAME;
        SQL_CREATE += " (" + TBL_COLUMN_ID;
        SQL_CREATE += " INTEGER PRIMARY KEY AUTOINCREMENT, ";
        SQL_CREATE += TBL_COLUMN_RFID + " TEXT, ";
        SQL_CREATE += TBL_COLUMN_START + " TEXT, ";
        SQL_CREATE += TBL_COLUMN_FINISH+ " TEXT)";

        return SQL_CREATE;
    }
}
*/
