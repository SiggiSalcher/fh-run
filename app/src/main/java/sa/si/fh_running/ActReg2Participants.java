package sa.si.fh_running;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;


import com.zebra.rfid.api3.ENUM_TRANSPORT;
import com.zebra.rfid.api3.ENUM_TRIGGER_MODE;
import com.zebra.rfid.api3.HANDHELD_TRIGGER_EVENT_TYPE;
import com.zebra.rfid.api3.InvalidUsageException;
import com.zebra.rfid.api3.OperationFailureException;
import com.zebra.rfid.api3.RFIDReader;
import com.zebra.rfid.api3.ReaderDevice;
import com.zebra.rfid.api3.Readers;
import com.zebra.rfid.api3.RfidEventsListener;
import com.zebra.rfid.api3.RfidReadEvents;
import com.zebra.rfid.api3.RfidStatusEvents;
import com.zebra.rfid.api3.START_TRIGGER_TYPE;
import com.zebra.rfid.api3.STATUS_EVENT_TYPE;
import com.zebra.rfid.api3.STOP_TRIGGER_TYPE;
import com.zebra.rfid.api3.TagData;
import com.zebra.rfid.api3.TriggerInfo;

import java.util.ArrayList;


public class ActReg2Participants extends AppCompatActivity {

    private String rfidCode=null;

    private static Readers readers;
    private static ReaderDevice readerDevice;
    private static RFIDReader reader;
    private static ArrayList<ReaderDevice> availableRFIDReaderList;

    //Handler

    //Eventhandler for RFID-Events
    EventHandlerReadingTagsReg eventHandler;

    //Handler for message transfer
    private Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message inMessage) {
            String msg = (String) inMessage.obj;
            boolean check=false;

            switch (inMessage.what) {
                case 30:
                    rfidCode=msg;
                    check=true;
                    break;

                default:
                    super.handleMessage(inMessage);
                    inMessage.recycle();
            }
            if(check){
                openActivity();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_reg_2_participants);


        //Toolbar
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar); //Getting own Toolbar
        TextView changeTitle = (TextView) toolbar.findViewById(R.id.toolbarTitle);
        ImageView logoChange=(ImageView) toolbar.findViewById(R.id.toolbarLogo);

        logoChange.setImageResource(R.drawable.fh_timing);
        changeTitle.setText("Registrierung");

        //Adding RFID-Reader and starting Eventlistening
        AddReader();

    }

    protected void openActivity() {
        disconReader();

        Intent inIntent=getIntent();

        Intent intent = new Intent (this, ActReg3Participants.class);
        intent.putExtra("TableName",inIntent.getStringExtra("TableName"));
        intent.putExtra("regFirstName",inIntent.getStringExtra("regFirstName"));
        intent.putExtra("regLastName",inIntent.getStringExtra("regLastName"));
        intent.putExtra("regAccID", inIntent.getStringExtra("regAccID"));
        intent.putExtra("regRFIDCode", rfidCode);

        startActivity(intent);
        finish();
        System.gc();
    }

    public void AddReader() {
        if (readers == null) {
            readers = new Readers(this, ENUM_TRANSPORT.BLUETOOTH);
        }

        new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... voids) {
                try {
                    if (readers != null) {
                        if (readers.GetAvailableRFIDReaderList() != null) {
                            availableRFIDReaderList = readers.GetAvailableRFIDReaderList();

                            if (availableRFIDReaderList.size() != 0) {
                                readerDevice = availableRFIDReaderList.get(0);
                                reader = readerDevice.getRFIDReader();

                                if (!reader.isConnected()) {
                                    reader.connect();
                                    ConfigureReader();
                                    return true;
                                }
                            }
                        }
                    }
                } catch (InvalidUsageException e) {
                    e.printStackTrace();
                } catch (OperationFailureException e) {
                    e.printStackTrace();
                    //Log.d(TAG, "OperationFailureException " + e.getVendorMessage());
                }
                return false;
            }
        }.execute();
    }

    private void ConfigureReader() {
        if (reader.isConnected()) {
            TriggerInfo triggerInfo = new TriggerInfo();

            triggerInfo.StartTrigger.setTriggerType(START_TRIGGER_TYPE.START_TRIGGER_TYPE_IMMEDIATE);
            triggerInfo.StopTrigger.setTriggerType(STOP_TRIGGER_TYPE.STOP_TRIGGER_TYPE_IMMEDIATE);

            try {
                //receive events from reader
                if(eventHandler == null) {
                    eventHandler=new EventHandlerReadingTagsReg(reader);
                }
                reader.Events.addEventsListener(eventHandler);
                reader.Events.setHandheldEvent(true); //Get Handheld events
                reader.Events.setTagReadEvent(true);    //Get Tag reading events
                reader.Events.setAttachTagDataWithReadEvent(false);

                reader.Config.setTriggerMode(ENUM_TRIGGER_MODE.BARCODE_MODE, false);
                reader.Config.setTriggerMode(ENUM_TRIGGER_MODE.RFID_MODE, true);    // set trigger mode as rfid -> disable barcode beams

                reader.Config.setStartTrigger(triggerInfo.StartTrigger);
                reader.Config.setStopTrigger(triggerInfo.StopTrigger);

            } catch (InvalidUsageException e) {
                e.printStackTrace();
            } catch (OperationFailureException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onDestroy() {
        disconReader();
        finish();
        System.gc();
        super.onDestroy();
    }

    protected void disconReader() {
        if(reader!=null) {
            boolean bCheck = true;
            reader.Actions.purgeTags();

            while(bCheck) {
                try {
                    reader.Config.setTriggerMode(ENUM_TRIGGER_MODE.RFID_MODE, false);
                    reader.Config.setTriggerMode(ENUM_TRIGGER_MODE.BARCODE_MODE, true);
                    bCheck = false;
                } catch (InvalidUsageException e) {
                    e.printStackTrace();
                } catch (OperationFailureException e) {
                    e.printStackTrace();
                }
            }
            bCheck = true;

            while (bCheck) {
                try {
                    reader.Events.removeEventsListener(eventHandler);
                    reader.disconnect();
                    reader = null;
                    readers.Dispose();
                    readers = null;
                    bCheck=false;

                    if (mHandler != null) {
                        mHandler = null;
                    }
                    if (eventHandler != null) {
                        eventHandler = null;
                    }
                } catch (InvalidUsageException e) {
                    e.printStackTrace();
                } catch (OperationFailureException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public class EventHandlerReadingTagsReg implements RfidEventsListener {

        private RFIDReader reader;

        public EventHandlerReadingTagsReg(RFIDReader readerIn) {
            this.reader=readerIn;
        }


        public void eventReadNotify(RfidReadEvents e) {
            TagData[] myTags = reader.Actions.getReadTags(1);

            if (myTags != null) {
                String msg = myTags[0].getTagID();
                mHandler.obtainMessage(30,msg).sendToTarget();
            }
        }

        //status event notification
        public void eventStatusNotify(RfidStatusEvents rfidStatusEvents) {

            if (rfidStatusEvents.StatusEventData.getStatusEventType() == STATUS_EVENT_TYPE.HANDHELD_TRIGGER_EVENT) {
                if(rfidStatusEvents.StatusEventData.HandheldTriggerEventData.getHandheldEvent() == HANDHELD_TRIGGER_EVENT_TYPE.HANDHELD_TRIGGER_PRESSED){

                    new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... voids) {
                            try {
                                reader.Actions.Inventory.perform();
                            } catch (InvalidUsageException e) {
                                e.printStackTrace();
                            } catch (OperationFailureException e){
                                e.printStackTrace();
                            }
                            return null;
                        }
                    }.execute();
                }

                if(rfidStatusEvents.StatusEventData.HandheldTriggerEventData.getHandheldEvent() == HANDHELD_TRIGGER_EVENT_TYPE.HANDHELD_TRIGGER_RELEASED){

                    new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... voids) {
                            try {
                                reader.Actions.Inventory.stop();
                            } catch (InvalidUsageException e) {
                                e.printStackTrace();
                            } catch (OperationFailureException e){
                                e.printStackTrace();
                            }
                            return null;
                        }
                    }.execute();
                }
            }
        }
    }
}




