package sa.si.fh_running;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.zebra.rfid.api3.BEEPER_VOLUME;
import com.zebra.rfid.api3.ENUM_TRIGGER_MODE;
import com.zebra.rfid.api3.InvalidUsageException;
import com.zebra.rfid.api3.OperationFailureException;
import com.zebra.rfid.api3.RFIDReader;
import com.zebra.rfid.api3.Readers;
import com.zebra.rfid.api3.RfidEventsListener;
import com.zebra.rfid.api3.RfidReadEvents;
import com.zebra.rfid.api3.RfidStatusEvents;
import com.zebra.rfid.api3.TagData;

import org.w3c.dom.Text;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import static java.lang.Thread.sleep;


public class ActStart2Event extends AppCompatActivity {

    //RFID-Reader Attributes
    private RFIDReader reader=null;
    private Readers readers;
    private EventHandlerReadingTagsStart eventHandler;

    //Attributes for Runner List
    private List<Runner_time_id_signal> time_id_signals;
    private Runner_time_id_signal help;

    //Location Services for next Activity
    private LocationManager locationManager;
    private LocationListener locationListener;

    //Attributes for Time-Delta-Calculation
    private String timeStamp;
    private long delta;

    //Eventname will be replaced!!!
    private String eventName="Event_23012019";

    //Database
    private RunnerDataSource dataSource;

    @SuppressLint("MissingPermission")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_start_2_event);

        //Toolbar
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar); //Getting own Toolbar

        //Setting title
        TextView test = (TextView) toolbar.findViewById(R.id.toolbarTitle);
        test.setText("Start Event");

        dataSource=new RunnerDataSource(this,eventName);

        //Getting Reader Informations from Activity before
        reader = ActStart1Event.reader;
        readers = ActStart1Event.readers;

        locationManager= (LocationManager) getSystemService(LOCATION_SERVICE);
        locationListener= new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                DateFormat dateFormat2=new SimpleDateFormat("HHmmssSSS", Locale.GERMANY);
                delta=Long.parseLong(dateFormat2.format(location.getTime())) - Long.parseLong(dateFormat2.format(Calendar.getInstance().getTime()));
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {
            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };

        //Getting new delta every 10 seconds
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10000, 0, locationListener);

        //Getting delta calculated in Activity before
        Intent intent = getIntent();
        delta = intent.getLongExtra("delta",0);

        //Configure Reader Device for new needs
        ConfigureReader();

        //Starting Inventory process to get TAG-Informations in EventHandlerReadingTagsStart
        try {
            reader.Actions.Inventory.perform();
        } catch (InvalidUsageException e1) {
            e1.printStackTrace();
        } catch (OperationFailureException e1) {
            e1.printStackTrace();
        }
    }

    public void butActStart2_next(View view) throws InvalidUsageException, OperationFailureException {
        //Buttons
        Button butActStart2stopEvent = (Button) findViewById(R.id.butActStart2_stopEvent);

        //Progress Bar
        ProgressBar progressBar=(ProgressBar) findViewById(R.id.progressBar);

        //Textview for Infotext
        TextView textView=(TextView)findViewById(R.id.textActStart2_notificationWait);

        locationManager.removeUpdates(locationListener);

        butActStart2stopEvent.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        textView.setVisibility(View.VISIBLE);


        reader.Actions.Inventory.stop();

        disconReader();

        locationManager.removeUpdates(locationListener);
        locationManager=null;
        locationListener=null;

        FillDatabase();
    }

    public void FillDatabase() {
        //ProgressBar
        ProgressBar progressBar =(ProgressBar)findViewById(R.id.progressBar);

        //Button
        Button butActStart2next =(Button) findViewById(R.id.butActStart2_nextAct);

        //TextView
        TextView textView = (TextView) findViewById(R.id.textActStart2_notificationWait);

        //Variables for Progressbar
        int     i=0,
                lengthOfList=time_id_signals.size();

        for (Runner_time_id_signal help : time_id_signals) {

            //Sort List by strongest Signal
            help.sortListTimeSignal_Signal();

            //setting StartTime calculated
            help.setStartTime(help.getTime());

            //Writing received Data into Database (RFID, StartingTime, 0 for FinishTime)
            try {
                dataSource.open();
                dataSource.createEntry(help.getRfidId(), help.getStartTime(), "0");
                dataSource.close();
                progressBar.setProgress((i++)/lengthOfList);

            } catch (Exception ex) {

            }

            //Output for check
            System.out.println("Signal: " + help.getSignal());
            System.out.println("Time: " + help.getTime());
            System.out.println("ID: " + help.getRfidId());

            //clear values
            help.clearTimeSignal();
        }

        //remove items from list
        time_id_signals.clear();

        progressBar.setVisibility(View.GONE);
        textView.setVisibility(View.GONE);
        butActStart2next.setVisibility(View.VISIBLE);
    }

    public void nextActFinish1(View view) {

        Intent intent = new Intent(this, ActFinish1Event.class);
        startActivity(intent);
    }

    private void ConfigureReader() {
        if (reader.isConnected()) {

            try {
                //receive events from device
                if(eventHandler == null) {
                    eventHandler=new EventHandlerReadingTagsStart(reader);
                }

                reader.Events.addEventsListener(eventHandler);
                reader.Events.setHandheldEvent(false); //Get Handheld events -> set to none
                reader.Events.setTagReadEvent(true);    //Get Tag reading events

                reader.Config.setTriggerMode(ENUM_TRIGGER_MODE.RFID_MODE, true);   // set trigger mode as rfid -> disable barcode beams
                reader.Config.setBeeperVolume(BEEPER_VOLUME.QUIET_BEEP);                            // set beeper to silent

            } catch (InvalidUsageException | OperationFailureException e) {
                e.printStackTrace();
            }
        }
    }

    protected void disconReader() {
        if (reader != null) {
            boolean bCheck = true;
            reader.Actions.purgeTags();
            int i=0;

            while (bCheck) {
                if(reader==null || i>100)
                    break;

                try {
                    reader.Config.setBeeperVolume(BEEPER_VOLUME.MEDIUM_BEEP);
                    reader.Config.setTriggerMode(ENUM_TRIGGER_MODE.RFID_MODE, false);
                    reader.Config.setTriggerMode(ENUM_TRIGGER_MODE.BARCODE_MODE, true);
                    bCheck = false;
                } catch (InvalidUsageException | OperationFailureException e) {
                    e.printStackTrace();
                }

                i++;
            }
            bCheck = true;
            i=0;

            while (bCheck) {

                if(reader==null || i >= 1000) {
                    break;
                }

                try {
                    reader.Events.removeEventsListener(eventHandler);
                    bCheck=false;

                } catch (InvalidUsageException | OperationFailureException e) {
                    e.printStackTrace();
                }

                if(reader.isConnected()&&!bCheck) {
                    try {
                        reader.disconnect();

                    } catch (InvalidUsageException | OperationFailureException e) {
                        e.printStackTrace();
                    }
                    readers.Dispose();
                }
                i++;
            }
            readers = null;
            reader = null;
        }
    }

    class EventHandlerReadingTagsStart implements RfidEventsListener {

        private RFIDReader reader;
        private DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss.SSS",Locale.GERMANY);

        Boolean bCheck=false;

        public EventHandlerReadingTagsStart(RFIDReader readerIn) {
            this.reader = readerIn;
        }

        @SuppressLint("MissingPermission")
        @Override
        public void eventReadNotify(RfidReadEvents rfidReadEvents) {

            TagData[] myTags = reader.Actions.getReadTags(1000);

            Runner_time_id_signal runner;
            boolean tagAd=true;

            //time calc with correction
            long timeCalc= Calendar.getInstance().getTimeInMillis() + delta;   //Getting system-time and adding correction to get GNSS time
            timeStamp=dateFormat.format(timeCalc);                                                  //setting readable format

            for(TagData actTag:myTags) {
                tagAd = true;
                runner = new Runner_time_id_signal(actTag.getTagID(), actTag.getPeakRSSI(), timeStamp);

                if (time_id_signals==null){
                    time_id_signals=new ArrayList<Runner_time_id_signal>();
                    time_id_signals.add(runner);
                }
                else {
                    for (Runner_time_id_signal runnerCheck : time_id_signals) {
                        if (runnerCheck.getRfidId().equals(actTag.getTagID())) {
                            runnerCheck.setNewTimeSignal(actTag.getPeakRSSI(), timeStamp);
                            tagAd=false;
                            break;
                        }
                    }
                    if(tagAd) {
                        time_id_signals.add(runner);
                    }
                }
            }

            try {
                sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
                Log.d("1","Problem in Sleep");
            }
        }

        @Override
        public void eventStatusNotify(RfidStatusEvents rfidStatusEvents) {

        }
    }
}