package sa.si.fh_running;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Runner_time_id_signal {
    private String rfid;
    private int dbId;
    private ArrayList<Runner_time_signal> timeSignalList; //time_signal_list
    private String runnerStartTime;
    private String runnerEndTime;

    Runner_time_id_signal() {

    }

    Runner_time_id_signal(String rfidIn, int signalStrengthIn, String timeIn) {
        this.rfid = rfidIn;
        if(timeSignalList==null) {
            this.timeSignalList = new ArrayList<Runner_time_signal>();
        }
        timeSignalList.add(new Runner_time_signal(signalStrengthIn, timeIn));
    }

    public void clearTimeSignal(){
        timeSignalList.clear();
    }

    public void setNewTimeSignal(int signalStrengthIn, String timeIn) {
        timeSignalList.add(new Runner_time_signal(signalStrengthIn, timeIn));
    }

    public int getDbId() {
        return this.dbId;
    }

    public void setDbId(int dbIdIn) {
        this.dbId=dbIdIn;
    }

    public String getRfidId() {
        return this.rfid;
    }

    public void setRfidId(String rfidIn) {
        this.rfid = rfidIn;
    }

    public String getStartTime() {
        return this.runnerStartTime;
    }

    public void setStartTime(String startTimeIn) {
        this.runnerStartTime = startTimeIn;
    }

    public String getFinishTime() {
        return this.runnerEndTime;
    }

    public void setFinishTime(String finishTimeIn) {
        this.runnerEndTime = finishTimeIn;
    }

    public void setFinishTimeAfterCalc() {
        this.runnerEndTime = calcAverageTime();
    }

    public String getSignal() {
        return timeSignalList.get(0).GetSignalStrength();
    }

    public String getTime() {
        return timeSignalList.get(0).GetTime();
    }

    public void sortListTimeSignal_Signal() {
        Collections.sort(timeSignalList);
    }

    public String calcAverageTime() {
        long timeFirst=getFirstTimeAppearanceLong();
        long timeLast=getLastTimeAppearanceLong();

        return calcTimeFromMillis(timeFirst+(timeLast-timeFirst)/2);
    }

    private long getFirstTimeAppearanceLong() {
        return calcTimeInMillis(timeSignalList.get(0).GetTime());
    }

    private long getLastTimeAppearanceLong() {
        long    signalLast=0,
                signalActual=0;

        for(int i=1;i<timeSignalList.size();i++) {
            signalActual=Long.parseLong(timeSignalList.get(i).GetSignalStrength());
            signalLast=Long.parseLong(timeSignalList.get(i-1).GetSignalStrength());

            if(signalActual != signalLast){
                return calcTimeInMillis(timeSignalList.get(i-1).GetTime());
            }

        }
        return calcTimeInMillis(timeSignalList.get(0).GetTime());
    }

    private long calcTimeInMillis(String timeIn) {
        //time In: HH:mm:ss.SSS
        int     iMilToSec = 1000,
                iSecToMin=iMilToSec*60,
                iMinToHour=iSecToMin*60;

        int     iHours = Integer.parseInt(timeIn.substring(0,2)),
                iMin = Integer.parseInt(timeIn.substring(3,5)),
                iSec = Integer.parseInt(timeIn.substring(6,8)),
                iMillis = Integer.parseInt(timeIn.substring(9));

        return iMillis+iSec*iMilToSec+iMin*iSecToMin+iHours*iMinToHour;
    }

    private String calcTimeFromMillis(long timeInMillis) {
        int     iHours=0,
                iMin=0,
                iSec=0,
                iMillis=0;

        DecimalFormat formatHMS = new DecimalFormat("00");
        DecimalFormat formatMillis = new DecimalFormat("000");

        //Calc Time out of milliseconds
        iHours= (int) (timeInMillis/3600000L);
        timeInMillis -= iHours*3600000;
        iMin= (int) (timeInMillis/60000L);
        timeInMillis -= iMin*60000;
        iSec = (int) (timeInMillis/1000L);
        iMillis = (int) timeInMillis - iSec*1000;

        //output HH:mm:ss.SSS
        return formatHMS.format(iHours)+":"+formatHMS.format(iMin)+":"+formatHMS.format(iSec)+"."+formatMillis.format(iMillis);
    }

    public String mergeTimeWithTrigger(List<String> triggerTime) {
        long    triggerTimeIn;

        if(!triggerTime.isEmpty()) {
            for (String timeOfTrigger : triggerTime) {
                triggerTimeIn = calcTimeInMillis(timeOfTrigger);

                if (getFirstTimeAppearanceLong() < triggerTimeIn && getLastTimeAppearanceLong() > triggerTimeIn) {
                    triggerTime.remove(triggerTime.indexOf(timeOfTrigger));
                    return timeOfTrigger;
                }
            }
        }
        return calcAverageTime();
    }
}

