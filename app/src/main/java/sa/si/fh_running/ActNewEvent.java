package sa.si.fh_running;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ActNewEvent extends AppCompatActivity {

    //content variables for DatabaseTransport
    public String eventName;
    public String eventPlace;
    public String eventDate;
    public String eventDist;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_new_event);

        //Toolbar
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar); //Getting own Toolbar
        TextView test = (TextView) toolbar.findViewById(R.id.toolbarTitle);
        test.setText("Anlegen");

        final EditText textEventName  = (EditText) findViewById(R.id.textInEventName);
        final EditText textEventPlace = (EditText) findViewById(R.id.textInPlace);
        final EditText textEventDate  = (EditText) findViewById(R.id.textInDate);
        final EditText textEventDist  = (EditText) findViewById(R.id.textInDist);

        //button
        Button buttonMain = (Button) findViewById(R.id.buttonMain);


        buttonMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //saving for later purpose
                eventName=textEventName.getText().toString();
                eventPlace=textEventPlace.getText().toString();
                eventDate=textEventDate.getText().toString();
                eventDist=textEventDist.getText().toString();

                if(!eventName.isEmpty()&&!eventPlace.isEmpty()&&!eventDate.isEmpty()&&!eventDist.isEmpty()){
                    openActMainMenue();
                }
                else {
                    Toast.makeText(ActNewEvent.this, "Please fill in all Informations",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void openActMainMenue() {
        Intent intent = new Intent(ActNewEvent.this, ActMainMenue.class);
        intent.putExtra("TableName",eventName);
        startActivity(intent);
    }
}
