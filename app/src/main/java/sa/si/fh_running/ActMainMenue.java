package sa.si.fh_running;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ActMainMenue extends AppCompatActivity implements View.OnClickListener {

    Button  butEventMenueRegister,
            butEventMenueStartEvent,
            butEventMenueResults;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_main_menue);

        //Toolbar
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar); //Getting own Toolbar
        TextView test = (TextView) toolbar.findViewById(R.id.toolbarTitle);
        ImageView logoChange=(ImageView) toolbar.findViewById(R.id.toolbarLogo);

        logoChange.setImageDrawable(Drawable.createFromPath("fh_timing"));
        test.setText("Event Menü");


        /*
        //buttons
        butEventMenueRegister = (Button) findViewById(R.id.butEventMenueRegister);
        butEventMenueStartEvent = (Button) findViewById(R.id.butEventMenueStartEvent);
        butEventMenueResults = (Button) findViewById(R.id.butEventMenueResults);

        //setting Listeners
        butEventMenueRegister.setOnClickListener(ActMainMenue.this);
        butEventMenueStartEvent.setOnClickListener(ActMainMenue.this);
        butEventMenueResults.setOnClickListener(ActMainMenue.this);
        */

    }

    @Override
    public void onClick(View view) {

        Intent intent=null;
        Intent inIntent=getIntent();

        switch (view.getId()){
            case R.id.butEventMenueRegister:
                intent = new Intent(ActMainMenue.this, ActReg1Participants.class);
                intent.putExtra("TableName",inIntent.getStringExtra("TableName"));
                openNewActivity(intent);
                break;
            case  R.id.butEventMenueStartEvent:
                Toast.makeText(ActMainMenue.this,"Start Event", Toast.LENGTH_SHORT).show();
                intent = new Intent(this, ActStart1Event.class);
                openNewActivity(intent);
                break;

            case R.id.butEventMenueResults:
                Toast.makeText(ActMainMenue.this,"Results", Toast.LENGTH_SHORT).show();
                //intent = new Intent(ActMainMenue.this, ActResults.class);
                break;
        }
        //openNewActivity(intent);

    }

    public void openNewActivity(Intent intent){
        startActivity(intent);
    }
}
