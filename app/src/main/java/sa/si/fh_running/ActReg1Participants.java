package sa.si.fh_running;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.zebra.scannercontrol.DCSSDKDefs;
import com.zebra.scannercontrol.DCSScannerInfo;
import com.zebra.scannercontrol.FirmwareUpdateEvent;
import com.zebra.scannercontrol.IDcsSdkApiDelegate;
import com.zebra.scannercontrol.SDKHandler;

import java.util.ArrayList;

import sa.si.fh_running.helpers.Barcode;

//import static sa.si.fh_running.Application.sdkHandler;

public class ActReg1Participants extends AppCompatActivity implements IDcsSdkApiDelegate {

    //Handler def
    private SDKHandler sdkHandler;
    private Handler mDataHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message inMessage) {
            String runnerInfo = (String) inMessage.obj;
            boolean check=false;

            switch (inMessage.what) {
                case 30:
                    check=getInfosBarcode(runnerInfo);
                    break;

                default:
                    super.handleMessage(inMessage);
            }

            if(check) {
                DisconReader(ScannerConn);
                openActivity();
            }
            else {
                Toast.makeText(ActReg1Participants.this,"Teilnehmercode falsch!",Toast.LENGTH_LONG).show();
            }
        }
    };

    ArrayList<DCSScannerInfo> mScannerInfoList=new ArrayList<>();
    //protected String RunnerInfos=null;
    protected int ScannerConn;
    protected String checkForContent="VN:";

    public String firstNameAct1="";
    public String lastNameAct1="";
    public String accountIDAct1="";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_reg_1_participants);

        //Toolbar
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar); //Getting own Toolbar
        TextView test = (TextView) toolbar.findViewById(R.id.toolbarTitle);
        test.setText("Registrierung");

        barcodeScanner();

    }

    //establish connection to Reader and Handle Events
    protected void barcodeScanner() {
        //SDKHandler
        sdkHandler=new SDKHandler(this);
        sdkHandler.dcssdkSetDelegate(this);

        int notifications_mask = 0;

        //Operational Mode
        sdkHandler.dcssdkSetOperationalMode(DCSSDKDefs.DCSSDK_MODE.DCSSDK_OPMODE_BT_NORMAL);

        //Subscripe to events
        notifications_mask=gettingSubScribeInfos(notifications_mask);

        // subscribe to events set in notification mask
        sdkHandler.dcssdkSubsribeForEvents(notifications_mask);


        //Connecting Scanner
        sdkHandler.dcssdkEnableAvailableScannersDetection(true);        //Detect Scanners

        sdkHandler.dcssdkGetAvailableScannersList(mScannerInfoList);    //Getting available Scanners (Scanners must be paired before

        DCSScannerInfo scannerInfo = mScannerInfoList.get(0);           //Getting first scanner out of Paired Readers
        ScannerConn=scannerInfo.getScannerID();                         //Getting Scanner ID

        connectToScanner(ScannerConn);                                  //Connecting to Scanner
    }

    //Opening new Activity and Handover the Data
    protected void openActivity() {
        //DisconReader();
        Intent inIntent=getIntent();
        Intent intent = new Intent (ActReg1Participants.this, ActReg2Participants.class); //Starting Act2Registration

        intent.putExtra("TableName",inIntent.getStringExtra("TableName"));
        intent.putExtra("regFirstName",firstNameAct1);
        intent.putExtra("regLastName",lastNameAct1);
        intent.putExtra("regAccID", accountIDAct1);

        startActivity(intent);
        finish();
        System.gc();
    }

    public boolean getInfosBarcode(String runnerInfos) {
        //Variables for checking the barcode content
        char[] cInfo = runnerInfos.toCharArray();
        char[] cCheck = checkForContent.toCharArray();
        boolean check = true;

        int iHelp = 0;
        //checking if content is correct (can be used in the Rest of the APP
        for (int i = 0; i < cCheck.length; i++) {
            if (cInfo[i] != cCheck[i]) {
                return false;
                //check = false;
                //break;
            }
        }

        //if correct split the string in substring (firstName, lastName, accountID)
        for (int i = 3; i < cInfo.length; i++) {
            if (cInfo[i] == 'N' && cInfo[i + 1] == 'N' && cInfo[i + 2] == ':' && cInfo[i + 3] == ' ') {
                firstNameAct1 = runnerInfos.substring(4, i);
                iHelp = i + 4;
            }
            if (cInfo[i] == 'I' && cInfo[i + 1] == 'D' && cInfo[i + 2] == ':' && cInfo[i + 3] == ' ') {
                lastNameAct1 = runnerInfos.substring(iHelp, i);
                accountIDAct1 = runnerInfos.substring(i + 4);
                break;
            }
        }
        return true;
    }
    //Standard Implements of IDcsSdkApiDelegate
    @Override
    public void dcssdkEventScannerAppeared(DCSScannerInfo dcsScannerInfo) {

    }

    @Override
    public void dcssdkEventScannerDisappeared(int i) {

    }

    @Override
    public void dcssdkEventCommunicationSessionEstablished(DCSScannerInfo dcsScannerInfo) {

    }

    @Override
    public void dcssdkEventCommunicationSessionTerminated(int i) {

    }

    //How to Act if Barcode Event appears
    @Override
    public void dcssdkEventBarcode(byte[] barcodeData, int barcodeType, int fromScannerID) {
        //getting barcode Infos from reader device
        Barcode barcode = new Barcode(barcodeData,barcodeType,fromScannerID);
        String bcRunnerInfo=new String(barcode.getBarcodeData());
        mDataHandler.obtainMessage(30,bcRunnerInfo).sendToTarget();
    }

    //Disconnect from Scanner
    public void DisconReader(int scannerID) {

        int notifications_mask = 0;

        if(sdkHandler!=null) {

            new MyAsynchTaskDisconnect(scannerID).execute();

            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            //Unsubscripe from events
            notifications_mask=gettingSubScribeInfos(notifications_mask);
            sdkHandler.dcssdkUnsubsribeForEvents(notifications_mask);

            sdkHandler.dcssdkEnableAvailableScannersDetection(false);        //Detect Scanners

            if(sdkHandler!=null){
                sdkHandler.dcssdkClose();
                sdkHandler = null;
            }

            if (mDataHandler!=null){
                mDataHandler=null;
            }
            ScannerConn=0;

        }
    }

    private int gettingSubScribeInfos(int notifications_mask) {

        // Subscribe to all barcode events
        notifications_mask |= DCSSDKDefs.DCSSDK_EVENT.DCSSDK_EVENT_BARCODE.value;

        return notifications_mask;
    }

    //Connecting to Scanner
    public void connectToScanner(int scannerID) {
        new MyAsyncTaskConnect(scannerID).execute();
    }

    //Asynch Task to deconnect from Scanner
    private class MyAsynchTaskDisconnect extends AsyncTask<Void,Integer,Boolean>{
        private int scannerID;

        public MyAsynchTaskDisconnect (int scannerIDIn) {
            this.scannerID=scannerIDIn;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {

            DCSSDKDefs.DCSSDK_RESULT result = DCSSDKDefs.DCSSDK_RESULT.DCSSDK_RESULT_FAILURE;

            if (sdkHandler != null) {
                result = sdkHandler.dcssdkTerminateCommunicationSession(scannerID);
                sdkHandler.dcssdkStopScanningDevices();
            }
            if(result == DCSSDKDefs.DCSSDK_RESULT.DCSSDK_RESULT_SUCCESS){
                return false;
            }
            else if(result == DCSSDKDefs.DCSSDK_RESULT.DCSSDK_RESULT_FAILURE) {
                return true;
            }
            return false;
        }
    }

    //Asynch Task to do not freeze the UI while connecting
    private class MyAsyncTaskConnect extends AsyncTask<Void,Integer,Boolean> {
        private int scannerId;
        public MyAsyncTaskConnect(int scannerId){
            this.scannerId=scannerId;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            DCSSDKDefs.DCSSDK_RESULT result = DCSSDKDefs.DCSSDK_RESULT.DCSSDK_RESULT_FAILURE;
            if (sdkHandler != null) {
                result = sdkHandler.dcssdkEstablishCommunicationSession(scannerId);
            }
            if(result == DCSSDKDefs.DCSSDK_RESULT.DCSSDK_RESULT_SUCCESS){
                return true;
            }
            else if(result == DCSSDKDefs.DCSSDK_RESULT.DCSSDK_RESULT_FAILURE) {
                return false;
            }
            return false;
        }
    }

    @Override
    public void dcssdkEventImage(byte[] bytes, int i) {

    }

    @Override
    public void dcssdkEventVideo(byte[] bytes, int i) {

    }

    @Override
    public void dcssdkEventBinaryData(byte[] bytes, int i) {

    }

    @Override
    public void dcssdkEventFirmwareUpdate(FirmwareUpdateEvent firmwareUpdateEvent) {

    }

    @Override
    public void dcssdkEventAuxScannerAppeared(DCSScannerInfo dcsScannerInfo, DCSScannerInfo dcsScannerInfo1) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        DisconReader(ScannerConn);
    }
}
